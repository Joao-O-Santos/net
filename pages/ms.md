# Linux Media Servers and Players

Notes on building Linux media servers and players.



## Performance

Video and audio performance are relative to the video and audio playback
device. Meaning, if the linux installation is configured to act as
a server the performance will be dependent on the client configuration
(e.g., an `Android` TV device). However, if the linux computer is acting
as the player these notes should apply. Despite the fact that `Android
TV` devices run a linux kernel, their theoretical performance may be
dependent on which configuration settings they expose to the user (in
non-rooted devices). Namely, they may not allow for **bit-perfect**
audio playback if there are no options to bypass `PulseAudio` sound
processing and use direct `ALSA` output to a DAC. Still, any differences
should be below the threshold of hearing and much smaller than the
distortion introduced by other components in the audio-chain. Video
performance for streaming services can actually be superior to
a standard linux install, not because of any technical superiority,
but because some services relying on DRM unfairly block 4k or even 1080p
playback on standard linux but not on `Android TV`.


### Audio

- With decent **asynchronous** USB DACs *any properly configured PC*
  can be an excellent source
  ([Archimago, 2018](https://archimago.blogspot.com/2018/12/measurements-intel-i7-pc-and-raspberry.html),
  see also, [Archimago, 2019](https://archimago.blogspot.com/2019/08/musings-demo-why-bits-are-bits-lets-not.html)).

- *Accurate* CD rips played with **bit-perfect** players are as good
  or better than any CD-transport paired with the *same (decent) DAC*.

- `ALSA` configured to output to DAC (should be **bit-perfect** and simple
  visual inspection of DAC screen should be enough to check
  **bit-depth** and **sample rate**).

- `apulse` to run `firefox` with `ALSA` instead of `pulseaudio` when
  streaming. The archwiki explains how to configure (`abou:config`
  changes) firefox for playback with `ALSA`.

- Realtime kernel isn't needed since USB DAC should be **asynchronous**
  anyway but it also shouldn't hurt. Setting low `nice` levels for sound
  processes are also unlikely to have an effect but they shouldn't hurt
  either.

- All this should be easy to set-up with some config files and shell
  scripting (possibly with `Ansible` as well).

- The above points are only applicable to if the computer is acting as
  a player, if it is acting as a server the performance is dependent on
  the client configuration. Note that `Android TV` devices may not allow
  for **bit-perfect** playback but should at least be compatible with
  DACs supported by the linux kernel (i.e., most DACs).


### Video

- With graphics cards from the last decade 1080p and even 4k playback
  shouldn't be problematic on linux. Still, `HDR` support is, at the
  time of writing, mostly lacking. Hardware acceleration for video
  playback may also be lacking depending on the player/browser used for
  playback.

- The latest versions of Nvidia Shield devices (running `Android Tv`)
  allow for AI powered 4k upscaling, that is likely better than the one
  provided by TVs and AVRs (see [LTT
  review](https://www.youtube.com/watch?v=NYfmxb1OqzE)).



## CD/DVD/BD Ripping

Ripping optical media to a computer's disks may act as backup, as well
as, streamline the playback process by making it compatible with
media server and playback software, foregoing the need for a dedicated
CD/DVD/BD player. Private copies of unencrypted discs are legal in
several jurisdictions but check your legislation. In no way is this
legal advice, but [in Portugal, private copies of DRM encrypted drives
should be legal](https://fsfe.org/news/2019/news-20191113-01.en.html).


### CDs

- [CDs don't have a proper filesystems so they should be ripped with
  appropriate software](https://bbs.archlinux.org/viewtopic.php?pid=1667905#p1667905).

- CD rip **accuracy** can be checked by comparing the checksums of the
  ripped CD files with checksums of the same files present in public
  databases. Notwithstanding, I suspect modern cd drives are probably
  accurate enough when operated with decent software and settings.

- `whipper` (uses `cdparanoia` + online drive offset databases + online
  cd checksum databases) or something simpler.

- Using `cdparanoia` or just `cdrtools`, coupled with `ffmpeg` to
  transcode, may be simpler and less overkill than `whipper`.

- See these Arch wiki entries for more
  [software options](https://wiki.archlinux.org/title/Optical_disc_drive#Ripping)
  and [instructions](https://wiki.archlinux.org/title/Rip_Audio_CDs).


### DVDs

- Use `ddrescue` transcode with `ffmpeg` or `HandBrake`
  (see [vlc docs](https://wiki.videolan.org/VLC_HowTo/Rip_a_DVD/#ddrescue_and_vobcopy)).

- For more options see the
  [arch wiki](https://wiki.archlinux.org/title/Optical_disc_drive#DVD-Video).

### Blu-Rays

- I'm unsure if the `ddrescue` method outline above should work for
  Blu-Ray disc backups but it should be worth the try.

- Blu-Ray playback may be a different beast but should still be possible
  with some fiddling around (see the [arch wiki](https://linustechtips.com/topic/1149230-will-ddrescue-recover-a-blu-ray-disk)).



## Software


### Players

- I prefer to use `mpv` but `vlc` and `mplayer` are also solid options.

-  For web playback `chromium` can output audio directly to `ALSA`, but
   you must use `apulse` to achieve the same with `firefox`.

- We browsers such as `chromium` and `firefox` should also be able to
  run at startup, and to run in kiosk mode, to work as sort of
  minimalist media center for (local and/or remote) streaming services.

- For a more comprehensive list of players see the arch wiki's list of
  [audio](https://wiki.archlinux.org/title/List_of_applications/Multimedia#Audio_players)
  and
  [video](https://wiki.archlinux.org/title/List_of_applications/Multimedia#Video_players)
  players.


### Servers

- `Jellyfin` seems to be all the rage these days, as FOSS media servers
  are concerned. I would like to try a more minimalist approach if
  possible. I'm considering `gerbera`, `rygel`, and `universal media
  server`. If I could expose a directory in the server and have the
  `mpv` `Android TV` app play the contents that would work to.

- When looking for just an audio server pure `mpd`, `volumio` or other
  audio servers are worth a look.

- For a more comprehensive list, once again, check the
  [arch wiki's list of applications](https://wiki.archlinux.org/title/List_of_applications/Multimedia#Media_servers).
