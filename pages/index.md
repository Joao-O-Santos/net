# Adventures in Networking

These are the notes from my journey planning, and hopefully setting up
a home network. The goal is for the network to feature some "production"
servers (e.g., [media server](./ms.html), NAS, build server) and a home
lab (e.g., testing server).

My goal is to keep updating this blog with notes and short reports from
my attempts.

I'm still on the planning phase and given my budget and time constrains
I expect it'll take some time to have even a small portion of the
network up and running.

Right now you can check out some of my [notes on setting up linux a media
servers and players](ms.html).
